const express = require('express');
const router = express.Router();
const User = require('../models/users');


router.get('/', async (req, res) => {
    const users = await User.find()
    res.json(users)
});

//buscar un solo id:
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const user = await User.findById(id);
    res.json(user);


});

router.post('/add', async (req, res) => {
    const { name, lastname } = req.body;
    const user = new User({ name, lastname });
    await user.save();
    res.json({ status: "add user" })
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const newUser = req.body;
    await User.findByIdAndUpdate(id, newUser);
    res.json({ status: "update user" })
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    await User.findByIdAndRemove(id);
    res.json({ status: "deleted user" })
});


module.exports = router;
