const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');

//db
require('./db');

//routes
const route = require('./routes');

//setting
app.set('port', process.env.PORT || 4000);

//staticfiles
app.use(express.static(path.join(__dirname, 'public')));

//middleware
app.use(morgan('dev'));
app.use(express.json());

//routes
app.use('/users/', route);

//listen
app.listen(app.get('port'), () => {
    console.log('listen on port')
});