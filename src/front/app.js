import React, { Component } from 'react';
import { render } from 'react-dom';

import Nav from './components/nav';
import ContainerUser from './components/ContainerUser';

class App extends Component {
    render() {
        return (
            <div>
                <Nav />

                <ContainerUser />
            </div>
        )
    }

}
export default App;