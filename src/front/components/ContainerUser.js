import React, { Component } from 'react';



class ContainerTask extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            lastname: '',
            _id: '',
            users: []




        },
            this.addUser = this.addUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.fetchUser = this.fetchUser.bind(this);



    };
    componentDidMount() {

        this.fetchUser()

    };
    fetchUser() {
        fetch('/users/').then(res => res.json())
            .then(data => this.setState({
                users: data
            }))

    };
    //Agregar tareas :
    addUser(e) {



        fetch('/users/add', {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: {
                'Accept': 'appication/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => console.log(data),
            M.toast({ html: 'Tarea guardada' }),
            this.setState({ name: '', lastname: '' }),
            this.fetchUser()

        )


            .catch(err => console.log(err));




        e.preventDefault();

    };

    //************************** */
    //actualizar 
    updateTask(id) {
        fetch(`/users/${id}`).then(res => res.json()).then(data => {
            this.setState({
                name: data.name,
                lastname: data.lastname,
                _id: data._id
            })
        })
        document.getElementById('update').style.display = 'initial';
        document.getElementById('sumitir').style.display = 'none';



    };

    //*****/************************************************ */
    editTask(id) {

        if (confirm('esta seguro de editar?')) {
            fetch(`/users/${id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'appication/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).then(data => console.log(data),
                M.toast({ html: 'Tarea Actualizada' })
            );




        }
        document.getElementById('update').style.display = 'none';
        document.getElementById('sumitir').style.display = 'initial';
        this.fetchUser();
    }




    //************************************************
    //Borrar  tareas:
    deleteTask(id) {
        if (confirm('esta seguro de eliminar?')) {
            fetch(`/users/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'appication/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).then(data => console.log(data),
                M.toast({ html: 'Tarea eliminada' }),
                this.fetchUser()
            );




        }
    }
    //*******************************************

    //capturar evento 
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }





    //fin funciones del formulario --------------------------------



    render() {
        return (
            <div className='container' >
                <div className='row' >
                    {/*Aqui comienza el formulario */}
                    <div className='col s5'>
                        <div className='card'>
                            <div className='card-content'>
                                <form onSubmit={this.addUser}>
                                    <div className='row'>
                                        <div className='input-field col s12'>
                                            <input type='text' name='name' placeholder='name' value={this.state.name} onChange={this.handleChange} />
                                        </div>
                                        <div className='input-field col s12'>
                                            <textarea placeholder='lastname' name='lastname' value={this.state.lastname} className='materialize-textarea' onChange={this.handleChange}></textarea>
                                        </div>
                                        <div  >
                                            <input type='submit' id='sumitir' value="send" className='btn light-blue darken-4' />
                                            <input type='button' id='update' value="update" style={{ display: 'none' }} className='btn light-lime darken-3' onClick={() => this.editTask(this.state._id)} />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    {/*Aqui termina el formulario */}

                    {/* Desde aqui la tabla */}

                    <div className='col s7'>
                        <table>
                            <thead>
                                <tr>
                                    <th>Titulo</th>
                                    <th>Descripcion</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.users.map((user) => {
                                    return (
                                        <tr key={user._id}>
                                            <td>{user.name}</td>
                                            <td>{user.lastname}</td>
                                            <td>
                                                <button className='btn light-blue darken-4 ' onClick={() => this.updateTask(user._id)}><i className='material-icons'>edit</i></button>
                                                <button className='btn light-blue darken-4 ' style={{ margin: '3px' }} onClick={() => this.deleteTask(user._id)}><i className='material-icons'>delete</i></button>

                                            </td>

                                        </tr>
                                    )
                                })
                                }
                            </tbody>
                        </table>

                    </div>



                    {/* Hasta aqui la tabla */}



                </div>
            </div>
        )
    }
};
export default ContainerTask;